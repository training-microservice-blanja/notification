package com.metraplasa.microservice.notification.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class KafkaSenderService {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Value("${kafka.topic.notifikasi.response}")
	private String notifikasiResponse;
	
	public void kirimStatus(String messageId, String status) {
		Map<String, String> hasil = new HashMap<>();
		hasil.put("status", status);
		hasil.put("i", messageId);
		
		String json;
		try {
			json = objectMapper.writeValueAsString(hasil);
			kafkaTemplate.send(notifikasiResponse, json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
