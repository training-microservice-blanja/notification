package com.metraplasa.microservice.notification.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metraplasa.microservice.notification.dto.EmailNotification;

@Service
public class KafkaListenerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@KafkaListener(topics = "${kafka.topic.notifikasi.request}")
	public void handleNotifikasi(String msg) {
		LOGGER.debug("Terima message : {}", msg);
		try {
			EmailNotification emailNotification = objectMapper.readValue(msg, EmailNotification.class);
			
			LOGGER.debug("Email Notification : {}", emailNotification);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
